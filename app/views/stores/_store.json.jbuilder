json.extract! store, :id, :name
json.shoes_inventory store.inventory
json.shoes store.shoes do |shoe|
  json.partial! 'shoes/shoe', shoe: shoe, store: store
end
