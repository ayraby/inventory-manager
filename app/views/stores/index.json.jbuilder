json.array! @stores do |store|
  json.partial! 'stores/store', store: store
end
