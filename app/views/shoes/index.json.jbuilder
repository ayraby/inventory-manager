json.array! @shoes do |shoe|
  json.partial! 'shoes/shoe', shoe: shoe, store: @store
end
