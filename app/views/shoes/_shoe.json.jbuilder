json.id shoe.id
json.name shoe.name
json.shoes shoe.inventory_by_store(store)
json.below_threshold shoe.is_below_threshold?(store)
json.below_restock_threshold shoe.is_below_restock_threshold?(store)
