class Shoe < ApplicationRecord
  has_many :store_shoes
  has_many :stores, through: :store_shoes

  def inventory_by_store(store)
    store_shoes.find_by!(store: store).quantity
  end

  def is_below_threshold?(store)
    inventory_by_store(store) < InventoryManager.threshold
  end

  def is_below_restock_threshold?(store)
    inventory_by_store(store) < InventoryManager.restock_threshold
  end
end
