class StoreShoe < ApplicationRecord
  belongs_to :store
  belongs_to :shoe
  after_update :notify_inventory_manager, if: :quantity_below_threshold?
  # after_update :automatic_restock, if: :quantity_below_restock_threshold?
  after_update :create_stat

  private

  def quantity_below_threshold?
    quantity < InventoryManager.threshold && !quantity_below_restock_threshold?
  end

  def quantity_below_restock_threshold?
    quantity < InventoryManager.restock_threshold
  end

  def create_stat
    Stat.create(store_shoe: self, quantity: quantity)
  end

  # def automatic_restock
  #   puts "Automatic restock triggered for #{shoe.name} in #{store.name}"
  #   quantity_to_restock = InventoryManager.threshold - quantity
  #   StoreShoe.where(shoe:).order("quantity DESC").each do |store_restock|
  #     if store_restock.quantity - quantity_to_restock > InventoryManager.threshold
  #       transfer = store_restock.quantity - quantity_to_restock
  #       store_restock.update(quantity: store_restock.quantity - transfer)
  #       update(quantity: quantity + transfer)
  #     end
  #   end
  # end
  # def automatic_restock
  #   puts "Automatic restock triggered for #{shoe.name} in #{store.name}"
  #   quantity_to_restock = InventoryManager.threshold - quantity
  #   StoreShoe.where(shoe:).order("quantity DESC").each do |store_restock|
  #     stock_after_transfer = store_restock.quantity - quantity_to_restock

  #     if stock_after_transfer >= InventoryManager.threshold
  #       store_restock.update(quantity: stock_after_transfer)
  #       update(quantity: quantity + quantity_to_restock)
  #       break
  #     else
  #       adjusted_transfer = InventoryManager.threshold - store_restock.quantity
  #       store_restock.update(quantity: InventoryManager.threshold)
  #       update(quantity: quantity + adjusted_transfer)
  #     end
  #   end
  # end

  # While this is not ideal to trigger the email from the model,
  # We keep it here for the sake of the exercise
  # Also it is easy to test the email using the console
  def notify_inventory_manager
    InventoryManager.handle_notification(store, shoe, quantity)
  end
end
