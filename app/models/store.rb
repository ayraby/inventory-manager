class Store < ApplicationRecord
  has_many :store_shoes
  has_many :shoes, through: :store_shoes

  def inventory
    store_shoes.sum(:quantity)
  end
end
