class ApplicationMailer < ActionMailer::Base
  default from: "aldo-inventory@localhost.com"
  layout "mailer"

  # Hi Karen!
  def notify_manager
    @store = params[:store]
    @shoe = params[:shoe]
    @quantity = params[:quantity]
    @recipient = "#{@store.name.downcase.parameterize(separator: '_')}-manager@localhost.com"
    mail(to: @recipient, subject: "Inventory Alert")
  end
end
