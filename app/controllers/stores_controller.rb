class StoresController < ApplicationController
  def index
    @stores = Store.all
    render do |format|
      format.json { render json: @stores }
      format.html { }
    end
  end

  def realtime
    @store = Store.find_by!(id: params[:store_id])
    @shoes = @store.shoes
    respond_to do |format|
      format.html
    end
  end

  def counter
    @store = Store.find(params[:store_id])
    render partial: "stores/counter", locals: { store: @store }
  end
end
