class StatsController < ApplicationController
  def show
    @stats = Stat.where(store_shoe_id: params[:id]).pluck(:created_at, :quantity).to_h
  end
end
