class ShoesController < ApplicationController
  before_action :set_store
  def index
    @shoes = @store.shoes
    respond_to do |format|
      format.json { render 'index', status: :ok }
      format.html
    end
  end

  private

  def set_store
    begin
      @store = Store.find_by!(id: params[:store_id])
    rescue ActiveRecord::RecordNotFound
      head :not_found
    end
  end
end
