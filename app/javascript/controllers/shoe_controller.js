import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="stores"
export default class extends Controller {
  static values = { url: String }

  connect() {
    this.refreshTimer = setInterval(() => {
      this.fetchAndUpdateCounters();
    }, 1000);
  }

  fetchAndUpdateCounters() {
    fetch(`${this.element.dataset.storesShoeUrl}.json`)
      .then(response => response.json())
      .then(data => {
        data.forEach(shoe => {
          const element = document.getElementById(`shoe_${shoe.id}`)
          const threshold = document.getElementById(`shoe_${shoe.id}_threshold`)
          const restock = document.getElementById(`shoe_${shoe.id}_restock_threshold`)
          if (element) {
            element.textContent = shoe.shoes
          }
          if (shoe.below_threshold && !shoe.below_restock_threshold) {
            threshold.classList.remove("hidden");
            threshold.classList.add("block");
          } else {
            threshold.classList.remove("block");
            threshold.classList.add("hidden");
          }

          if (shoe.below_restock_threshold) {
            restock.classList.remove("hidden");
            restock.classList.add("block");
          } else {
            restock.classList.remove("block");
            restock.classList.add("hidden");
          }
        })
      })
      .catch(error => console.error("Failed to fetch and update counter:", error));
  }

  disconnect() {
    clearInterval(this.refreshTimer);
  }
}
