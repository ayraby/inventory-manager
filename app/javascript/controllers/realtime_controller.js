import { Controller } from "@hotwired/stimulus"

export default class extends Controller {

  initialize() {
    this.socket = new WebSocket('ws://0.0.0.0:8081/')
  }
  connect() {
    this.socket.onopen = () => {
      console.log("Connected to the WebSocket server");
    };

    this.socket.onmessage = (event) => {
      const data = JSON.parse(event.data);
      if (data.store === this.element.dataset.store) {
        this.updateCounters(data);
      }
    };

    this.socket.onerror = (error) => {
      console.error("WebSocket error:", error);
    };
  }

  updateCounters({ model, inventory }) {
    const element = document.getElementById(`shoe_${model.toLowerCase()}`)
    const threshold = document.getElementById(`shoe_${model.toLowerCase()}_threshold`)
    const restock = document.getElementById(`shoe_${model.toLowerCase()}_restock_threshold`)
    if (element) {
      element.textContent = inventory
    }
    if (inventory < 70 && inventory > 20) {
      this.showThreshold(threshold, element);
      this.hideRestock(restock, element);
    }

    if (inventory < 20) {
      this.showRestock(restock, element);
      this.hideThreshold(threshold, element);
    }

    if (inventory > 70) {
      this.hideRestock(restock, element);
      this.hideThreshold(threshold, element);
    }
  }

  showThreshold(threshold, element) {
    threshold.classList.remove("hidden");
    threshold.classList.add("block");
    element.closest('li').classList.add("blink-threshold");
  }

  hideThreshold(threshold, element) {
    threshold.classList.add("hidden");
    threshold.classList.remove("block");
    element.closest('li').classList.remove("blink-threshold");
  }

  showRestock(restock, element) {
    restock.classList.remove("hidden");
    restock.classList.add("block");
    element.closest('li').classList.add("blink-restock");
  }

  hideRestock(restock, element) {
    restock.classList.add("hidden");
    restock.classList.remove("block");
    element.closest('li').classList.remove("blink-restock");
  }

  disconnect() {}
}
