import { Controller } from "@hotwired/stimulus"
// import { Turbo } from "@hotwired/turbo-rails"

// Connects to data-controller="stores"
export default class extends Controller {
  static values = { url: String }

  connect() {
    this.refreshTimer = setInterval(() => {
      this.fetchAndUpdateCounters();
    }, 1000);
  }

  fetchAndUpdateCounter() {
    fetch(this.element.dataset.storeCounterUrl)
      .then(response => response.text())
      .then(text => document.getElementById(this.element.id).textContent = text )
      .catch(error => console.error("Failed to fetch and update counter:", error));
  }

  fetchAndUpdateCounters() {
    fetch('/stores.json')
      .then(response => response.json())
      .then(data => {
        data.forEach(store => {
          const element = document.getElementById(`store_${store.id}_count`)
          if (element) {
            element.textContent = store.shoes_inventory
          }
        })
      })
      .catch(error => console.error("Failed to fetch and update counter:", error));
  }

  disconnect() {
    clearInterval(this.refreshTimer);
  }
}
