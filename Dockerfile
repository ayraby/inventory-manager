FROM ruby:3.1.2 AS rails-template

LABEL maintainer="Alexandre-Yoann Raby"

ENV APP_PORT ${APP_PORT}
ENV BUNDLE_PATH="/usr/local/bundle"

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .

EXPOSE ${APP_PORT}

CMD ["bash", "entrypoint.sh"]
