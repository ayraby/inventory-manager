require 'json'

class InventoryManager
  class << self
    def threshold
      50
    end

    def restock_threshold
      20
    end

    def handle_message(message)
      data = JSON.parse(message)
      store = Store.find_by(name: data['store'])
      unless store.nil?
        shoe = Shoe.find_by(name: data['model'])
        unless shoe.nil?
          store_shoe = store.store_shoes.find_by(shoe: shoe)
          store_shoe.update(quantity: data['inventory'])
        end
      end
    end

    def handle_notification(store, shoe, quantity)
      ApplicationMailer.with(store:, shoe:, quantity:).notify_manager.deliver_now
    end
  end
end
