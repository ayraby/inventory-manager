class AddStats < ActiveRecord::Migration[7.0]
  def change
    create_table :stats do |t|
      t.references :store_shoe, foreign_key: true, null: true

      t.string :quantity
      t.timestamps
    end
  end
end
