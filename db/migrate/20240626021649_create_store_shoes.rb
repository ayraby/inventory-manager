class CreateStoreShoes < ActiveRecord::Migration[7.0]
  def change
    create_table :store_shoes do |t|
      t.references :store, null: false, foreign_key: true
      t.references :shoe, null: false, foreign_key: true
      t.integer :quantity, default: 0

      t.timestamps
    end
  end
end
