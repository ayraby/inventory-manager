# frozen_string_literal: true
%w[ADERI MIRIRA CAELAN BUTAUD SCHOOLER SODANO MCTYRE CADAUDIA RASIEN WUMA GRELIDIEN CADEVEN SEVIDE ELOILLAN BEODA
    VENDOGNUS ABOEN ALALIWEN GREG BOZZA].map do |shoe_name|
      Shoe.find_or_create_by(name: shoe_name).persisted?
    end

    puts 'Shoes created'
    puts "#{Shoe.count} shoes in the database"

[
  'ALDO Centre Eaton', 'ALDO Destiny USA Mall', 'ALDO Pheasant Lane Mall', 'ALDO Holyoke Mall',
  'ALDO Maine Mall', 'ALDO Crossgates Mall', 'ALDO Burlington Mall', 'ALDO Solomon Pond Mall',
  'ALDO Auburn Mall', 'ALDO Waterloo Premium Outlets'
].map do |store_name|
  store = Store.find_or_create_by(name: store_name)
  store.shoes << Shoe.all
end

puts 'Stores created'
puts "#{Store.count} stores in the database"
