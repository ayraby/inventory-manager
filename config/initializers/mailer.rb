Rails.application.configure do
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    address: ENV.fetch('SMTP_HOST') { 'maildev' },
    port: ENV.fetch('SMTP_PORT') { '1025' },
    domain: ENV.fetch('SMTP_DOMAIN') { 'localhost' },
    user_name: nil,
    password: nil,
    authentication: nil,
    enable_starttls_auto: false
  }
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default_url_options = {
    host: ENV.fetch('API_HOST') { 'localhost' },
    port: ENV.fetch('DOMAIN_PORT') { '3000' }
  }

  routes.default_url_options = {
    host: ENV.fetch('API_HOST') { 'localhost' },
    port: ENV.fetch('DOMAIN_PORT') { '' }
  }
end
