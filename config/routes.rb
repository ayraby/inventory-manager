Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"

  # This could have been a solution but I would like to use it as a reference
  # namespace :api do
  #   namespace :v1 do
  #     resources :stores, only: [:index, :show] do
  #       resources :shoes, only: [:index, :show]
  #     end
  #   end
  # end

  root "stores#index"
  resources :stores, only: [:index] do
    get 'counter', to: 'stores#counter'
    get 'realtime'
    resources :shoes, only: [:index]
  end
  resources :stats, only: [:show]
end
