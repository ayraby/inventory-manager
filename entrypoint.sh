#!/bin/bash
set -e
echo "Environment: $RAILS_ENV"

bin/rails db:prepare
if [ -f $APP_PATH/tmp/pids/server.pid ]; then
  rm -f $APP_PATH/tmp/pids/server.pid
fi

bin/rails assets:precompile
bin/rails s -p $APP_PORT -b '0.0.0.0'

# Then exec the container's main process (what's set as CMD in the Dockerfile).
exec "$@"
